<html>
<head>
    <title>Calculator</title>
</head>
<body>

<form method="post" action="form1.php">
    <p align="center">
        <b>Enter the first number : </b> <br>
        <input type="number" name="num1"> <br>
    </p>

    <p align="center">
        <select name="function">
            <option value="+">Add [+]</option>
            <option value="-">Substract [-]</option>
            <option value="*">Multiple [*]</option>
            <option value="/">Divide [/]</option>
        </select>
    </p>

    <p align="center">
        <b>Enter the second number : </b> <br>
        <input type="number" name="num2">
    </p>

    <p align="center">
        <b>Answer : </b> <br>
        <input type="number" name="ans" value="<?php isset($request) ? $request : null;?>" readonly>
    </p>

    <p align="center">
        <input type="submit" value="Calculate" name="calculate">
    </p>
</form>

</body>
</html>