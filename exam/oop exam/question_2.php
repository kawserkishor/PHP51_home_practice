<?php
interface User
{
	public function setUsername($username);
	public function getUsername();
	public function setGender($gender);
	public function getGender();
}

class Commentator implements User
{
	public $username;
	public $gender;

	public function setUsername($username){
		$this->username = $username;
		return $this;
	}
	public function getUsername(){
		return $this->username;
	}
	public function setGender($gender){
		$this->gender = $gender;
		return $this;
	}
	public function getGender(){
		return $this->gender;
	}

	public function addTitle(){
		switch ($this->gender) {
			case 'male':
				$this->username = "Mr. ".$this->username;
				break;
			
			case 'female':
				$this->username = "Mrs. ".$this->username;
				break;
		}
	}
}

//Instantiate Comentator
$comentrator = new Commentator();
$comentrator->setGender('female')->setUsername('Jane')->addTitle();
echo "Username is : ".$comentrator->getUsername()."<br>";
$comentrator->setGender('male')->setUsername('Bob')->addTitle();
echo "Username is : ".$comentrator->getUsername();