<?php

abstract class User
{
	protected $scores = 0;
	protected $numberOfArticles = 0;

	public function setNumberOfArticles($int){
		if (is_int($int)) {
			$this->numberOfArticles = $int;
		}else{
			throw new Exception("Value {$int} must be Integer", 1);
			
		}
	}

	public function getNumberOfArticles(){
		return $this->numberOfArticles;
	}

	abstract public function calcScores();
}

class Author extends User
{
	public function calcScores(){
		$this->scores = $this->getNumberOfArticles() * 10 + 20;
		return $this->scores;
	}
}

class Editor extends User
{
	public function calcScores(){
		$this->scores = $this->getNumberOfArticles() * 6 + 15;
		return $this->scores;
	}
}

//Instantiate Class
$author1 = new Author();
$author1->setNumberOfArticles(8);
echo "Total Author Score :".$author1->calcScores();
echo "<br>";
//Instantiate Editor Class
$editor1 = new Editor();
$editor1->setNumberOfArticles(15);
echo "Total Author Score :".$editor1->calcScores();