<?php
    interface User{
        public function setUserName($userName);
    }

    trait Writing{
        public function writeContent(){
            echo get_class($this).", please start typing ".$this->action;
        }
    }

    class Author implements User{
        use Writing;
        protected $username;
        protected $action = "an Article";
        public function setUserName($userName)
        {
            $this->username = $userName;
        }
    }
    class Commentator implements User{
        use Writing;
        protected $username;
        protected $action = "your Comment";
        public function setUserName($userName)
        {
            $this->username = $userName;
        }
    }
    class Viewer implements User{
        use Writing;
        protected $username;
        public function setUserName($userName)
        {
            $this->username = $userName;
        }
        public function writeContent(){
        }
    }

    $author1 = new Viewer();
    $author1->writeContent();
    ?>