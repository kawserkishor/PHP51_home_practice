<?php

trait Price{
    public function changePriceByDollars($price, $change){
        return $price + $change." ";
    }
}

trait SpecialSell{
    public function announceSpecialSell(){
        return __CLASS__." on special sell";
    }
}

class Mercedes{
    use Price;
    use SpecialSell;
}

$mercedes1 = new Mercedes();
echo $mercedes1->changePriceByDollars(42000, -2100);
echo $mercedes1->announceSpecialSell();