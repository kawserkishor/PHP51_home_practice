<?php
interface User{

    public function setProperty($username, $gender);
    public function getProperty();
}

class Commentator implements User{
    public $username;
    public $gender;

    public function setProperty( $username, $gender)
    {
        $this->username = $username;
        $this->gender = $gender;
        return $this;
    }


    public function addTitle(){
        if ($this->gender == 'male'){
            $this->username = "Mr. ".$this->username;
        }
        else if ($this->gender == 'female'){
            $this->username = "Mrs. ".$this->username;
        }
    }
    public function getProperty()
    {
        return $this->username;
    }

}

$user1 = new Commentator();
$user1->setProperty('Bob', 'male')->addTitle();
echo $user1->getProperty();
echo "<br>";
// $user1 = new Commentator();
 $user1->setProperty('Jane', 'female')->addTitle();
 echo $user1->getProperty();