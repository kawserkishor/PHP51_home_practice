<?php

interface User{
    public function serUserName($userName);
}

trait Writing{
    public function writeContent(){
        echo get_class($this).", please start typing an article...";
    }
}



class Author implements User{
    use Writing;
    public function writeContent()
    {
        return "Author, please start typing an article...";
    }
}

class Commentator implements User{

}

class Viewer implements User{

}

$author = new Author();
echo $author->writeContent();