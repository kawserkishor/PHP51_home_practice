<?php
$a = 0;
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = 42;
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = 0.0;
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = 4.2;
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = "";
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = "string";
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = "0";
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = "1";
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = 0;
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = [1,2];
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
$a = [];
echo $a.'     '.(boolval($a) ? 'true' : 'false').'<br>';
