(function($){
	$(".tabs a").click(function(event) {
		event.preventDefault();
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
		var tab = $(this).attr("href");
		$(".tab_contant").not(tab).css("display", "none");
		$(tab).fadeIn();
	});
}) ( jQuery );
