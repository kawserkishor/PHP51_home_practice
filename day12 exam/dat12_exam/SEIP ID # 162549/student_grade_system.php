<?php
include "result_sheet.php";
function getGrade($mark){
    if ($mark >= 90){
        return "A+";
    }
    else if($mark >= 70 && $mark <= 89){
        return "B+";
    }
    else if($mark >= 50 && $mark <= 69){
        return "C+";
    }
    else{
        return "F";
    }
}
$tempMark = $studentMarks[$_GET['studentID']];
echo "Result (Grade)  "." : ".$_GET['studentID']." is ".getGrade(intval($tempMark));
?>