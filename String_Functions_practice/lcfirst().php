<?php
$foo = 'HelloWorld';
$foo = lcfirst($foo);

echo $foo.'<br>';

$bar = 'Hello World!';
$bar = lcfirst($bar);

echo '<br>'.$bar.'<br>';

$bar = lcfirst(strtoupper($bar));

echo '<br>'.$bar.'<br>';


// Exercise 2

$string = 'CamelCase';
$string{0} = strtolower($string{0}); // didn't understand this line
echo '<br>'.$string;