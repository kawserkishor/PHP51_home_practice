<?php

class Car{
    public $color ='Green';
    public $price=10.00;
    public $model='';
    public $picture;
    protected $door = 4;
    private $seat = 6;
    private $fuel = 10;

    public function run(){
        $this->fuel = $this->fuel - 1;
        return "I am running";
    }

    public function fill($liter){
        $this->fuel += $liter;
    }


    public function getFuel()
    {
        return $this->fuel;
    }

}
$car1 = new Car;
$car2 = new Car();

echo $car1->color;
$car1->color = "Red";
echo "<br />";
echo $car1->color;
echo "<br />";
echo $car1->price;
echo "<br />";
//echo $car1->seat;
echo "<br />";
//echo $car1->door;
//echo $car1->run();
echo "<br />";
echo $car1->getFuel();
$car1->fill(100);
echo "<br />";
echo $car1->run();
echo $car1->run();
echo $car1->run();
echo $car1->getFuel();
