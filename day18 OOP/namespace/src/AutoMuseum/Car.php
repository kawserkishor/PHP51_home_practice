<?php

namespace Basis\AutoMuseum;
class Car{
    public $color ='Green';
    public $price=10.00;
    public $model='';
    public $picture;
    protected $door = 4;
    private $seat = 6;
    private $fuel = 10;

    public function run(){
        $this->fuel = $this->fuel - 1;
        return "I am running";
    }

    public function fill($liter){
        $this->fuel += $liter;
    }


    public function getFuel()
    {
        return $this->fuel;
    }

}
