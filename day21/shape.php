<?php

abstract class Shape{

    abstract function getArea();
}
class Circle extends Shape{
    public $radious;

    public function getArea()
    {
        //parent::getArea();
        $r = $this->radious;
        $area = 2*3.1416*$r;
        return 'Area of circle : '.$area."<br>";
    }
}
class Rectangle extends Shape{
    public $length;
    public $width;

    public function getArea()
    {
        $area = $this->length * $this->width;
        return 'Area of Rectangle : '.$area."<br>";
    }
}
class Triagle extends Shape{
    public $height;
    public $width;

    public function getArea()
    {
        $area = 1/2*($this->width*$this->height);
        return 'Area of Triangle : '.$area."<br>";
    }
}

$circle = new Circle();
$circle->radious = 4;
echo $circle->getArea();

$rectangle = new Rectangle();
$rectangle->length = 5;
$rectangle->width = 3;
echo $rectangle->getArea();

$triangle = new Triagle();
$triangle->height = 3;
$triangle->width = 5;
echo $triangle->getArea();





