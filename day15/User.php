<?php


class User
{
private $userName;

    public function setter($userName){
        $this->userName = $userName;            // declered variable and the name of the object after $this-> must be same
                                               //& parameter of method and variable name after object must be same
    }


    public function getUserName()
    {
        $userName= $this->userName;
        return $userName;
    }

}

class Admin extends User
{
    public function ofexpressYourRole(){
        $admin = "Admin";
        return $admin;
    }

    public function sayHello() {
        $user = $this->getUserName();
        $message = "Hello admin, ".$user;
        return $message;
    }
}
$admin1 = new Admin();
$admin1->setter("Balthazar");
echo $admin1->sayHello();
