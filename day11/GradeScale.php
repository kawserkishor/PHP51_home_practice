<?php


class GradeScale
{
    public $mathInput;
    public $engInput;
    public $bengInput;
    public $phyInput;

    public function __construct($mathInput, $engInput, $bengInput, $phyInput)
    {
        $this->mathInput = $mathInput;
        $this->engInput = $engInput;
        $this->bengInput = $bengInput;
        $this->phyInput = $phyInput;
    }

    public function totalMarks()
    {
        $marks = $this->mathInput + $this->engInput + $this->bengInput + $this->phyInput;
        return $marks;

    }

    public function grade()
    {
        if(empty($this->mathInput )|| empty($this->engInput ) || empty($this->bengInput ) || empty($this->phyInput )){
            echo "Please proveide with marks";
        }
        else if($this->mathInput < 70 || $this->engInput < 70 || $this->bengInput < 70 || $this->phyInput < 70 ){
            echo "Grade: Fail";
        }
        else if($this->mathInput >= 90 && $this->engInput >= 90 && $this->bengInput >= 90 && $this->phyInput >= 90){
            echo "Grade: Golden A+";
        }
        else if($this->mathInput >= 80 && $this->engInput >= 80 && $this->bengInput >= 80 && $this->phyInput >= 80){
            echo "Grade: A+";
        }

        else{
            echo "Grade: A";
        }
    }
}

$result1 = new GradeScale($_POST['math'],$_POST['eng'],$_POST['beng'],$_POST['phy']);

echo "Total Marks : ";
echo $result1->totalMarks()."<br>";

echo $result1->grade();
//echo $result1->grade();